<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Playlist;

class PlaylistController extends Controller
{
    public function displayPlaylist(){
        return DB::table('Playlist')->get();
    }

    public function store(Request $request){

        $newPlaylist = new Playlist();
        $newPlaylist->name = $request->name;
        $newPlaylist->save();
        return $newPlaylist;
    }
}
