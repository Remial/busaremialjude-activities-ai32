<?php
use App\Http\Controllers\SongController;
use App\Http\Controllers\PlaylistController;
use App\Http\Controllers\PlaylistSongController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/songs', [SongController::class, 'displaySongs']);
Route::post('/upload', [SongController::class, 'store']);

Route::get('/playlist', [PlaylistController::class, 'displayPlaylists']);
Route::post('/create', [PlaylistController::class, 'store']);

Route::get('/playlistsongs', [PlaylistSongController::class, 'displayPlaylistSongs']);
Route::post('/create', [PlaylistSongController::class, 'store']);