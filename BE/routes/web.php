<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/songs', [SongController::class, 'displaySongs']);
Route::post('/upload', [SongController::class, 'store']);

Route::get('/playlist', [PlaylistController::class, 'displayPlaylists']);
Route::post('/create', [PlaylistController::class, 'store']);

Route::get('/playlistsongs', [PlaylistSongController::class, 'displayPlaylistSongs']);
Route::post('/create', [PlaylistSongController::class, 'store']);